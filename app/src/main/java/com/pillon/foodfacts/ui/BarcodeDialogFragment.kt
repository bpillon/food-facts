package com.pillon.foodfacts.ui

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.pillon.foodfacts.misc.BarcodeListener
import com.pillon.foodfacts.R
import kotlinx.android.synthetic.main.fragment_dialog_barcode.*
import java.lang.NumberFormatException

class BarcodeDialogFragment : DialogFragment() {

  private var listener: BarcodeListener? = null

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
    inflater.inflate(R.layout.fragment_dialog_barcode, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    submit.setOnClickListener {
      onSubmit()
    }
  }

  private fun onSubmit() {
    try {
      val barcode = barcodeField.text.toString().toLong()
      listener?.onBarcodeSubmitted(barcode)
      dismiss()
    } catch (e: NumberFormatException) {
      Toast.makeText(context, getString(R.string.wrong_barcode_format), Toast.LENGTH_SHORT).show()
    }
  }

  fun setBarcodeListener(listener: BarcodeListener) {
    this.listener = listener
  }
}