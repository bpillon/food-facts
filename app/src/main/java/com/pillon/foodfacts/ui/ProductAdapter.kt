package com.pillon.foodfacts.ui

import android.support.v7.recyclerview.extensions.AsyncListDiffer
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.support.v7.util.DiffUtil
import com.pillon.foodfacts.data.models.Product
import com.pillon.foodfacts.misc.ProductClickedListener
import com.pillon.foodfacts.R
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.adapter_product.*

class ProductAdapter(private val listener: ProductClickedListener? = null) :
  RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

  val diffItemCallback = object : DiffUtil.ItemCallback<Product>() {
    override fun areItemsTheSame(oldProduct: Product, newProduct: Product) = oldProduct.id == newProduct.id

    override fun areContentsTheSame(oldProduct: Product, newProduct: Product) =
      oldProduct.lastSeen == newProduct.lastSeen
  }

  val differ = AsyncListDiffer<Product>(this, diffItemCallback)

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
    return ProductViewHolder(
      LayoutInflater.from(parent.context)
        .inflate(R.layout.adapter_product, parent, false)
    )
  }

  fun submitProducts(products: List<Product>) {
    differ.submitList(products)
  }

  private fun getProduct(position: Int) = differ.currentList[position]

  override fun getItemCount() = differ.currentList.size

  override fun onBindViewHolder(holder: ProductViewHolder, position: Int) = holder.bind(getProduct(position), listener)

  class ProductViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    fun bind(product: Product, listener: ProductClickedListener?) = with(itemView) {
      nameView.text = product.name

      setOnClickListener {
        listener?.onProductClicked(product)
      }
    }
  }
}
