package com.pillon.foodfacts.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import com.pillon.foodfacts.*
import com.pillon.foodfacts.data.FoodFactsCallback
import com.pillon.foodfacts.data.models.Product
import com.pillon.foodfacts.data.ProductViewModel
import com.pillon.foodfacts.misc.BarcodeListener
import com.pillon.foodfacts.misc.showDialog

import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call

class MainActivity : AppCompatActivity(), BarcodeListener {

  private val viewModel by lazy {
    ViewModelProviders.of(this).get(ProductViewModel::class.java)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    setSupportActionBar(toolbar)

    supportFragmentManager.beginTransaction()
      .replace(R.id.mainContainer, ProductsFragment()).commit()

    // if land layout
    if (detailContainer != null) {
      supportFragmentManager.beginTransaction()
        .replace(R.id.detailContainer, ProductDetailFragment()).commit()
    }

    viewModel.selectedProduct.observe(this, Observer<Product> { product ->
      onSelectedProduct(product)
    })

    fab.setOnClickListener {
      AddBarcodeChooserDialogFragment().showDialog(supportFragmentManager)
    }
  }

  override fun onAttachFragment(fragment: Fragment) {
    when (fragment) {
      is CameraFragment -> {
        fragment.setBarcodeListener(this)
      }
      is BarcodeDialogFragment -> {
        fragment.setBarcodeListener(this)
      }
    }
  }

  override fun onBarcodeSubmitted(barcode: Long) {
    getProductById(barcode)
  }

  override fun onSupportNavigateUp(): Boolean {
    onBackPressed()
    if (supportFragmentManager.backStackEntryCount == 0) handleBack(false)
    return true
  }

  private fun onSelectedProduct(product: Product?) {
    product?.let { p ->

      // if detail fragment already exists
      val productDetailFragment =
        supportFragmentManager.findFragmentById(R.id.detailContainer) as? ProductDetailFragment
          ?: supportFragmentManager.findFragmentById(R.id.mainContainer) as? ProductDetailFragment

      productDetailFragment?.let { fragment ->
        fragment.updateSelectedProduct(p)
      } ?: run {
        handleBack(true)
        supportFragmentManager.beginTransaction()
          .replace(
            R.id.mainContainer,
            ProductDetailFragment.newInstance(p)
          ).addToBackStack(null).commit()
      }
    }
  }

  private fun getProductById(id: Long) {
    progress.visibility = VISIBLE
    viewModel.get(id, object : FoodFactsCallback<Product>() {
      override fun onFailure(call: Call<Product>, t: Throwable) {
        progress.visibility = GONE
        Toast.makeText(this@MainActivity, getString(R.string.product_not_found), Toast.LENGTH_SHORT).show()
      }

      override fun onSuccess(t: Product) {
        progress.visibility = GONE
      }
    })
  }

  private fun handleBack(visible: Boolean) {
    supportActionBar?.setDisplayHomeAsUpEnabled(visible)
  }
}
