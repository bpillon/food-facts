package com.pillon.foodfacts.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v4.app.Fragment
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_products.*
import android.support.v7.widget.DividerItemDecoration
import android.view.View.GONE
import android.view.View.VISIBLE
import com.pillon.foodfacts.data.models.Product
import com.pillon.foodfacts.misc.ProductClickedListener
import com.pillon.foodfacts.data.ProductViewModel
import com.pillon.foodfacts.R

class ProductsFragment : Fragment(), ProductClickedListener {

  private val adapter = ProductAdapter(this)
  private var firstLoad = true

  private val viewModel by lazy {
    activity?.run {
      ViewModelProviders.of(this).get(ProductViewModel::class.java)
    } ?: throw Exception("Invalid Activity")
  }

  private val hasLandLayout by lazy {
    activity?.supportFragmentManager?.findFragmentById(R.id.detailContainer) as? ProductDetailFragment != null
  }

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? = inflater.inflate(R.layout.fragment_products, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    list.layoutManager = LinearLayoutManager(context)
    list.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
    list.adapter = adapter

    viewModel.allProducts.observe(this, Observer<List<Product>> { products ->
      if (firstLoad && hasLandLayout) {
        firstLoad = false
        viewModel.select(products?.firstOrNull())
      }

      products?.let {
        adapter.submitProducts(it)
        emptyState.visibility = if (it.isEmpty()) VISIBLE else GONE
      }
    })
  }

  override fun onProductClicked(product: Product) {
    viewModel.select(product)
  }
}
