package com.pillon.foodfacts.ui

import android.graphics.Typeface.BOLD
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.text.SpannableStringBuilder
import android.text.Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bumptech.glide.Glide
import com.pillon.foodfacts.misc.PARCELABLE_PRODUCT
import com.pillon.foodfacts.data.models.Product
import com.pillon.foodfacts.R
import com.pillon.foodfacts.misc.log
import kotlinx.android.synthetic.main.fragment_product_detail.*

class ProductDetailFragment : Fragment() {

  companion object {
    fun newInstance(product: Product) = ProductDetailFragment().apply {
      arguments = Bundle(1).apply {
        putParcelable(PARCELABLE_PRODUCT, product)
      }
    }
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    val product = arguments?.getParcelable<Product>(PARCELABLE_PRODUCT)

    product?.let {
      updateSelectedProduct(product)
    }
  }

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? = inflater.inflate(R.layout.fragment_product_detail, container, false)

  fun updateSelectedProduct(product: Product) {
    Glide.with(this).load(product.imageUrl).thumbnail(0.1f).into(imageView)

    nameView.text = product.name
    ingredientsView.setTextWithBoldPrefix(product.ingredientsText, R.string.ingredients)
    energyView.setTextWithBoldPrefix(product.energy, R.string.energy)
  }

  private fun TextView.setTextWithBoldPrefix(text: String?, @StringRes prefixResId: Int) {
    text?.let {
      val prefix = getString(prefixResId)
      val value = SpannableStringBuilder("$prefix $it")
      value.setSpan(StyleSpan(BOLD), 0, prefix.length, SPAN_EXCLUSIVE_EXCLUSIVE)
      setText(value)
    }
  }
}