package com.pillon.foodfacts.ui

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.pillon.foodfacts.misc.BarcodeListener
import com.pillon.foodfacts.R
import kotlinx.android.synthetic.main.fragment_camera.*

class CameraFragment : Fragment() {

  private var listener: BarcodeListener? = null

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? = inflater.inflate(R.layout.fragment_camera, container, false)

  override fun onResume() {
    super.onResume()
    cameraView.onResume()
  }

  override fun onPause() {
    cameraView.onPause()
    super.onPause()
  }

  override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    cameraView.onRequestPermissionsResult(requestCode, permissions, grantResults)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    fabTakePhoto.setOnClickListener { captureImage() }

    btnRetry.setOnClickListener {
      if (cameraView.visibility == View.VISIBLE) handlePreview(true) else handlePreview(false)
    }
  }

  private fun handlePreview(visible: Boolean) {
    framePreview.visibility = if (visible) VISIBLE else GONE
    cameraView.visibility = if (visible) GONE else VISIBLE
  }

  private fun captureImage() {
    fabProgressCircle.show()
    cameraView.captureImage { _, bytes ->
      val bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)

      getQRCodeDetails(bitmap)
      activity?.runOnUiThread {
        handlePreview(true)
        imagePreview.setImageBitmap(bitmap)
      }
    }
  }

  fun setBarcodeListener(listener: BarcodeListener) {
    this.listener = listener
  }

  private fun getQRCodeDetails(bitmap: Bitmap) {
    val options = FirebaseVisionBarcodeDetectorOptions.Builder()
      .setBarcodeFormats(
        FirebaseVisionBarcode.FORMAT_ALL_FORMATS
      )
      .build()
    val detector = FirebaseVision.getInstance().getVisionBarcodeDetector(options)
    val image = FirebaseVisionImage.fromBitmap(bitmap)
    detector.detectInImage(image)
      .addOnSuccessListener {
        it.firstOrNull()?.displayValue?.toLong()?.let { barcode ->
          handleReceivedBarcode(barcode)
        }
      }
      .addOnFailureListener {
        it.printStackTrace()
        Toast.makeText(context, getString(R.string.error_placeholder), Toast.LENGTH_SHORT).show()
      }
      .addOnCompleteListener {
        fabProgressCircle?.hide()
      }
  }

  private fun handleReceivedBarcode(barcode: Long) {
    listener?.onBarcodeSubmitted(barcode)
    fragmentManager?.popBackStack()
  }
}