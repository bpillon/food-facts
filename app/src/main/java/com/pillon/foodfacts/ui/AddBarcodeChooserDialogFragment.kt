package com.pillon.foodfacts.ui

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pillon.foodfacts.R
import com.pillon.foodfacts.misc.displayAsDialog
import com.pillon.foodfacts.misc.showDialog
import kotlinx.android.synthetic.main.adapter_dialog_barcode_chooser.view.*
import kotlinx.android.synthetic.main.fragment_products.*

class AddBarcodeChooserDialogFragment : DialogFragment() {

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
    inflater.inflate(R.layout.fragment_dialog_barcode_chooser, container, false)

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    activity?.let {
      list.layoutManager = LinearLayoutManager(context)
      list.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
      list.adapter = TypeAdapter(
        arrayOf(
          Type.Camera(this),
          Type.TextField(this)
        )
      )
    }
  }

  sealed class Type {
    data class Camera(val dialogFragment: DialogFragment) : Type() {
      override val text: String
        get() = dialogFragment.getString(R.string.camera)
      override val callback: () -> Unit
        get() = {
          dialogFragment.dismiss()
          CameraFragment().displayAsDialog(dialogFragment.activity)
        }
    }

    data class TextField(val dialogFragment: DialogFragment) : Type() {
      override val text: String
        get() = dialogFragment.getString(R.string.text)

      override val callback: () -> Unit
        get() = {
          BarcodeDialogFragment().showDialog(dialogFragment.activity?.supportFragmentManager)
        }
    }

    abstract val text: String
    abstract val callback: () -> Unit
  }

  private class TypeAdapter(private val types: Array<Type>) : RecyclerView.Adapter<TypeAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
      return ViewHolder(
        LayoutInflater.from(parent.context)
          .inflate(R.layout.adapter_dialog_barcode_chooser, parent, false)
      )
    }

    override fun getItemCount() = types.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(types[position])

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
      fun bind(type: Type) = with(itemView) {
        textView.text = type.text

        setOnClickListener {
          type.callback()
        }
      }
    }
  }

}