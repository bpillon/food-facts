package com.pillon.foodfacts.misc

import android.support.v4.app.*
import android.util.Log
import com.pillon.foodfacts.R
import com.pillon.foodfacts.data.models.Product
import java.io.Serializable

fun String.log() {
  Log.i(LOG_TAG, this)
}

fun DialogFragment.showDialog(fragmentManager: FragmentManager?) {
  val ft = fragmentManager?.beginTransaction()
  val prev = fragmentManager?.findFragmentByTag(DIALOG_TAG)
  if (prev != null) {
    ft?.remove(prev)
  }
  setStyle(DialogFragment.STYLE_NO_TITLE, R.style.CustomDialog)
  show(ft, DIALOG_TAG)
}

fun Fragment.displayAsDialog(activity: FragmentActivity?) {
  val fragmentManager = activity?.supportFragmentManager

  val transaction = fragmentManager?.beginTransaction()
  transaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
  transaction?.add(android.R.id.content, this)?.addToBackStack(null)?.commit()
}

fun <A, B, R> Pair<A?, B?>?.letNotNull(action: (pair: Pair<A, B>) -> R): R? {
  if (this?.first != null && second != null)
    return action(Pair(first!!, second!!)) else return null
}

interface BarcodeListener : Serializable {
  fun onBarcodeSubmitted(barcode: Long)
}

interface ProductClickedListener : Serializable {
  fun onProductClicked(product: Product)
}