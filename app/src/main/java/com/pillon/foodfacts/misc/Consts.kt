package com.pillon.foodfacts.misc

val PARCELABLE_PRODUCT = "PARCELABLE_PRODUCT"
val LOG_TAG = "FOOD_FACTS_LOG"
val DIALOG_TAG = "DIALOG"
val DATABASE_NAME = "product_database"
val BASE_API_URL = "https://world.openfoodfacts.org/api/v0/"