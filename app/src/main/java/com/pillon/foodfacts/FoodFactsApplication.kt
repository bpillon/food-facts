package com.pillon.foodfacts

import android.app.Application
import com.google.firebase.FirebaseApp
import com.pillon.foodfacts.data.FoodFactsService
import com.pillon.foodfacts.data.ProductJsonAdapter
import com.pillon.foodfacts.misc.BASE_API_URL
import retrofit2.Retrofit
import com.squareup.moshi.Moshi
import retrofit2.converter.moshi.MoshiConverterFactory


class FoodFactsApplication : Application() {

  companion object {
    val foodFactsService by lazy {
      val retrofit = Retrofit.Builder()
        .baseUrl(BASE_API_URL)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .build()

      retrofit.create<FoodFactsService>(FoodFactsService::class.java)
    }

    val moshi by lazy {
      Moshi.Builder().add(ProductJsonAdapter()).build()
    }
  }

  override fun onCreate() {
    super.onCreate()
    FirebaseApp.initializeApp(this)
  }

}