package com.pillon.foodfacts.data

import com.pillon.foodfacts.data.models.Product
import com.pillon.foodfacts.misc.letNotNull
import com.squareup.moshi.*

class ProductJsonAdapter : JsonAdapter<Product>() {

  @FromJson
  override fun fromJson(reader: JsonReader): Product? = parse(reader)

  companion object {
    val PRODUCT_NAME = "product"
    val NAMES = JsonReader.Options.of("id", "product_name", "image_url", "ingredients_text", "nutriments")
    val NUTRIMENTS_NAMES = JsonReader.Options.of("energy_unit", "energy")
  }

  fun parse(reader: JsonReader): Product? {
    var id: Long = -1L
    var name = ""
    var imageUrl: String? = null
    var ingredientsText: String? = null
    var energyUnit: String? = null
    var energyValue: Int? = null

    reader.readObject {
      when (reader.nextName()) {
        PRODUCT_NAME -> {
          reader.readObject {
            when (reader.selectName(NAMES)) {
              0 -> id = reader.nextLong()
              1 -> name = reader.nextString()
              2 -> imageUrl = reader.nextString()
              3 -> ingredientsText = reader.nextString()
              4 -> reader.readObject {
                when (reader.selectName(NUTRIMENTS_NAMES)) {
                  0 -> energyUnit = reader.nextString()
                  1 -> energyValue = reader.nextInt()
                  else -> reader.skipNameAndValue()
                }
              }
              else -> reader.skipNameAndValue()
            }
          }
        }
        else -> reader.skipValue()
      }
    }

    if (id == -1L || name == "") {
      throw JsonDataException("Missing required field")
    }

    var energy: String?

    energy = Pair(energyValue, energyUnit).letNotNull {
      "${it.first} ${it.second}"
    }

    return Product(id, name, imageUrl, ingredientsText, energy)
  }

  override fun toJson(writer: JsonWriter, value: Product?) {
    // TODO
  }
}

fun JsonReader.skipNameAndValue() {
  skipName()
  skipValue()
}

inline fun JsonReader.readObject(body: () -> Unit) {
  beginObject()
  while (hasNext()) {
    body()
  }
  endObject()
}