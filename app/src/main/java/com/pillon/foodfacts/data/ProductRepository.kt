package com.pillon.foodfacts.data

import android.app.Application
import android.arch.lifecycle.LiveData
import com.pillon.foodfacts.data.models.Product
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class ProductRepository internal constructor(application: Application) {

  private val productDao: ProductDao
  internal val allProducts: LiveData<List<Product>>

  init {
    val db = ProductRoomDatabase.getInstance(application)
    productDao = db.productDao()
    allProducts = productDao.allProducts
  }

  fun loadProductWithId(id: Long, observer: MaybeObserver<Product>) {
    productDao.loadProductWithId(id).subscribeOn(Schedulers.io())
      .observeOn(AndroidSchedulers.mainThread()).subscribe(observer)
  }

  fun updateLastSeenForProduct(
    product: Product,
    lastSeen: Long = System.currentTimeMillis(),
    observer: SingleObserver<Any>? = null
  ) {
    product.lastSeen = lastSeen

    val single = Single.fromCallable {
      productDao.updateProduct(product)
    }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

    if (observer != null) single.subscribe(observer) else single.subscribe()

  }

  fun insert(product: Product) {
    Completable.fromAction { productDao.insert(product) }.subscribeOn(Schedulers.io()).subscribe()
  }
}