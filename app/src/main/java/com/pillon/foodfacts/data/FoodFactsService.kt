package com.pillon.foodfacts.data

import com.pillon.foodfacts.data.models.Product
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path


interface FoodFactsService {
  @GET("product/{productId}.json")
  fun getProduct(@Path("productId") productId: Long): Call<Product>
}