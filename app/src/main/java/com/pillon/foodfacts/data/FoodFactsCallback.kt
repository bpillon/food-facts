package com.pillon.foodfacts.data

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

abstract class FoodFactsCallback<T> : Callback<T> {
  override fun onFailure(call: Call<T>, t: Throwable) {
    // TODO : Handle failure
  }

  override fun onResponse(call: Call<T>, response: Response<T>) {
    response.body()?.let { onSuccess(it) } ?: onError(response.errorBody())
  }

  open fun onSuccess(t: T) {
    // Do something with result
  }

  open fun onError(errorBody: ResponseBody?) {
    // TODO : Handle error
  }
}