package com.pillon.foodfacts.data

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.pillon.foodfacts.FoodFactsApplication
import com.pillon.foodfacts.data.models.Product
import io.reactivex.MaybeObserver
import io.reactivex.disposables.Disposable
import retrofit2.Call


class ProductViewModel(application: Application) : AndroidViewModel(application) {

  private val repository = ProductRepository(application)
  val allProducts: LiveData<List<Product>> = repository.allProducts
  val selectedProduct: MutableLiveData<Product> = MutableLiveData()

  fun get(productId: Long, callback: FoodFactsCallback<Product>? = null) {
    repository.loadProductWithId(productId, getProductObserver(productId, callback))
  }

  private fun getProductObserver(productId: Long, callback: FoodFactsCallback<Product>? = null) =
    object : MaybeObserver<Product> {
      override fun onSuccess(p: Product) {
        // Product found
        select(p)
        repository.updateLastSeenForProduct(p)
        callback?.onSuccess(p)
      }

      override fun onComplete() {
        // Product not found in DB
        request(productId, callback)
      }

      override fun onSubscribe(d: Disposable) {}

      override fun onError(e: Throwable) {
        // Todo : Handle error
      }
    }

  fun request(productId: Long, callback: FoodFactsCallback<Product>? = null) {
    FoodFactsApplication.foodFactsService.getProduct(productId).enqueue(object : FoodFactsCallback<Product>() {
      override fun onSuccess(product: Product) {
        insert(product)
        select(product)

        callback?.onSuccess(product)
      }

      override fun onFailure(call: Call<Product>, t: Throwable) {
        callback?.onFailure(call, t)
      }
    })
  }

  fun insert(product: Product) {
    repository.insert(product)
  }

  fun select(product: Product?) {
    product.let {
      selectedProduct.value = it
    }
  }


}