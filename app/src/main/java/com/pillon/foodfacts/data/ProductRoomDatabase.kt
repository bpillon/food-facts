package com.pillon.foodfacts.data

import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.Database
import android.content.Context
import com.pillon.foodfacts.data.models.Product
import com.pillon.foodfacts.misc.DATABASE_NAME

@Database(entities = [Product::class], version = 1)
abstract class ProductRoomDatabase : RoomDatabase() {

  abstract fun productDao(): ProductDao

  companion object {

    @Volatile
    private var INSTANCE: ProductRoomDatabase? = null

    fun getInstance(context: Context): ProductRoomDatabase =
      INSTANCE ?: synchronized(this) {
        INSTANCE
          ?: buildDatabase(context).also { INSTANCE = it }
      }

    private fun buildDatabase(context: Context) =
      Room.databaseBuilder(
        context.applicationContext,
        ProductRoomDatabase::class.java, DATABASE_NAME
      )
        .fallbackToDestructiveMigration()
        .build()
  }
}