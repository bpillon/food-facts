package com.pillon.foodfacts.data

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.pillon.foodfacts.data.models.Product
import io.reactivex.Maybe


@Dao
interface ProductDao {

  @get:Query("SELECT * from products ORDER BY last_seen DESC")
  val allProducts: LiveData<List<Product>>

  @Query("SELECT * FROM products WHERE id = :id")
  fun loadProductWithId(id: Long): Maybe<Product>

  @Update
  fun updateProduct(product: Product)

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  fun insert(product: Product)

  @Query("DELETE FROM products")
  fun deleteAll()
}