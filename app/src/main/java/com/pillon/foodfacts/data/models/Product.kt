package com.pillon.foodfacts.data.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "products")
data class Product(
  @PrimaryKey var id: Long,
  var name: String?,
  @ColumnInfo(name = "image_url") var imageUrl: String?,
  @ColumnInfo(name = "ingredients_text") var ingredientsText: String?,
  @ColumnInfo(name = "energy") var energy: String? = null,
  @ColumnInfo(name = "last_seen") var lastSeen: Long = System.currentTimeMillis()
) : Parcelable